import User, { IUser } from './user';

interface IRoom {
  name: string;
  users: User[];
}

export default class Rooms {
  rooms: IRoom[];
  users: User[];

  constructor() {
    this.rooms = [];
    this.users = [];
  }

  createRoom = (roomName: string): IRoom => {
    const newRoom = { name: roomName, users: [] };

    this.rooms.push(newRoom);
    return newRoom;
  };

  userJoinRoom = (roomName: string, user: User): IRoom | undefined => {
    const room = this.getRoomByName(roomName);
    if (room) {
      room.users.push(user);
      this.users.push(user);
    }

    return room;
  };

  userLeaveRoom = (roomName: string, user: User): IRoom | undefined => {
    const room = this.getRoomByName(roomName);
    if (room) {
      const index = room.users.findIndex((u) => u.id === user.id);
      if (index !== -1) {
        room.users.splice(index, 1)[0];
      }
    }
    return room;
  };

  getRoomByName = (roomName: string): IRoom | undefined => {
    return this.rooms.find((room) => room.name === roomName);
  };

  getUserById = (id: string): User | undefined => {
    return this.users.find((user) => user.id === id);
  };

  getAllRooms = (): IRoom[] => this.rooms;
}
