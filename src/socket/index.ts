import { Server } from 'socket.io';
import * as config from './config';
import User from '../utils/user';
import Rooms from '../utils/rooms';

const rooms = new Rooms();

export default (io: Server) => {
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username;

    socket.emit('UPDATE_ROOMS', rooms.getAllRooms());

    socket.on('CREATE_ROOM', (roomName, username) => {
      const room = rooms.getRoomByName(roomName);

      if (!room) {
        rooms.createRoom(roomName);
        io.emit('UPDATE_ROOMS', rooms.getAllRooms());
        io.to(roomName).emit('USERS_JOINED', roomName, username);
      } else {
        socket.emit('ROOM_EXISTS', roomName);
      }
    });

    socket.on('JOIN_ROOM', (roomName, username) => {
      socket.join(roomName);

      const user = new User(username);
      user.setId(socket.id);
      user.setRoom(roomName);

      rooms.userJoinRoom(roomName, user);

      const userList = rooms.getRoomByName(roomName)?.users;

      io.emit('UPDATE_USERS_COUNT', roomName, userList?.length);

      io.to(roomName).emit('USERS_JOINED', roomName, userList);
    });

    socket.on('TOGGLE_READY', (username) => {
      const user = rooms.getUserById(socket.id);
      if (user) {
        user.setReady(!user.isReady);
        const userList = rooms.getRoomByName(user.room)?.users;

        io.to(user.room).emit('USERS_JOINED', user.room, userList);

        const isNotReady = userList?.filter((user) => !user.isReady);

        // TODO
        if (!isNotReady?.length) {
          console.log('ALL is ready');
        }
      }
    });

    socket.on('disconnect', () => {
      const user = rooms.getUserById(socket.id);
      if (user) {
        const roomName = user.room;
        rooms.userLeaveRoom(roomName, user);
        const userList = rooms.getRoomByName(roomName)?.users;

        io.to(roomName).emit('USERS_JOINED', roomName, userList);
      }
    });
  });
};
