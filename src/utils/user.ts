export interface IUser {
  username: string;
  id: string;
  room: string;
  setId: (id: string) => void;
  setRoom: (name: string) => void;
  setReady: (ready: boolean) => void;
}

export default class User implements IUser {
  username: string;
  id: string;
  room: string;
  isReady: boolean;

  constructor(name) {
    this.username = name;
    this.id = '';
    this.room = '';
    this.isReady = false;
  }

  setId(id: string) {
    this.id = id;
  }

  setRoom(name: string) {
    this.room = name;
  }

  setReady(ready: boolean) {
    this.isReady = ready;
  }
}
