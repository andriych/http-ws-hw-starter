import { showInputModal } from './views/modal.mjs';
import {
  appendRoomElement,
  updateNumberOfUsersInRoom,
  removeRoomElement,
} from './views/room.mjs';

import {
  appendUserElement,
  changeReadyStatus,
  setProgress,
  removeUserElement,
} from './views/user.mjs';

const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const usersWrapper = document.getElementById('users-wrapper');
const roomsWrapper = document.getElementById('rooms-wrapper');
const roomNameField = document.getElementById('room-name');
const quitRoomBtn = document.getElementById('quit-room-btn');
const readyBtn = document.getElementById('ready-btn');

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

const addRoomButton = document.getElementById('add-room-btn');

socket.on('connect', () => {});

let roomName = '';

addRoomButton.addEventListener('click', () => {
  showInputModal({
    title: 'Add room',
    onChange: (name) => {
      roomName = name;
    },
    onSubmit: () => {
      socket.emit('CREATE_ROOM', roomName, username);
      socket.emit('JOIN_ROOM', roomName, username);
    },
  });
});

socket.on('UPDATE_ROOMS', (rooms) => {
  roomsWrapper.innerHTML = '';
  rooms.forEach((room) => {
    appendRoomElement({
      name: room.name,
      numberOfUsers: room.users.length,
      onJoin: () => {
        socket.emit('JOIN_ROOM', room.name, username);
      },
    });
  });
});

socket.on('UPDATE_USERS_COUNT', (roomName, usersCount) => {
  updateNumberOfUsersInRoom({ name: roomName, numberOfUsers: usersCount });
});

socket.on('USERS_JOINED', (roomName, users) => {
  const hide = ['full-screen', 'display-none'];
  roomsPage.classList.add(...hide);
  gamePage.classList.remove(...hide);

  usersWrapper.innerHTML = '';
  users.forEach((user) =>
    appendUserElement({
      username: user.username,
      ready: user.isReady,
      isCurrentUser: user.username === username,
    })
  );
  roomNameField.textContent = roomName;
});

socket.on('ROOM_EXISTS', (room) => {
  alert(`${room} already exists`);
});

quitRoomBtn.addEventListener('click', () => {
  window.location.replace('/login');
});

readyBtn.addEventListener('click', () => {
  socket.emit('TOGGLE_READY', username);
});
